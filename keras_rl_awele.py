import warnings

from rl.random import OrnsteinUhlenbeckProcess

warnings.filterwarnings("ignore")
with warnings.catch_warnings():
    warnings.filterwarnings("ignore", category=FutureWarning)

import logging

logging.getLogger('tensorflow').disabled = True
from keras.callbacks import TensorBoard
from keras import Sequential, Model, Input
from keras.layers import Flatten, Dense, concatenate
from keras.optimizers import Adam
from rl.agents import DQNAgent, SARSAAgent, DDPGAgent
from rl.memory import SequentialMemory
from rl.policy import LinearAnnealedPolicy, EpsGreedyQPolicy, BoltzmannQPolicy

import gym
from gym_awele.envs import AwelePlayer, BotAwelePlayer, Awele_Board, AweleEnv

import matplotlib.pyplot as plt
import numpy as np


def build_actor_model(action_size, observation_shape):
    action_input = Input(shape=(1,) + observation_shape)
    x = Flatten()(action_input)
    x = Dense(16, activation="relu")(x)
    x = Dense(16, activation="relu")(x)
    x = Dense(16, activation="relu")(x)
    x = Dense(action_size, activation="linear")(x)
    actor = Model(inputs=action_input, outputs=x)
    print(actor.summary())
    return actor


def build_critic_model(action_shape, observation_shape):
    action_input = Input(shape=action_shape)
    observation_input = Input(shape=(1,) + observation_shape)
    flattened_observation = Flatten()(observation_input)
    x = concatenate([action_input, flattened_observation])
    x = Dense(32, activation="relu")(x)
    x = Dense(32, activation="relu")(x)
    x = Dense(32, activation="relu")(x)
    x = Dense(1, activation="linear")(x)
    critic = Model(inputs=[action_input, observation_input], outputs=x)
    print(critic.summary())
    return critic, action_input


def build_model(state_size, num_actions):
    model = Sequential()
    model.add(Flatten(input_shape=(1, state_size)))
    model.add(Dense(state_size * 2, activation='relu'))
    model.add(Dense(state_size * 2, activation='relu'))
    model.add(Dense(state_size * 2, activation='relu'))
    model.add(Dense(num_actions, activation='linear'))
    print(model.summary())
    return model


def build_agent_sarsa(states_size, actions_size, policy):
    model = build_model(states_size, actions_size)
    sarsa = SARSAAgent(model=model, nb_actions=actions_size, nb_steps_warmup=10, gamma=.99, test_policy=None,
                       policy=policy, train_interval=1)
    return sarsa


def build_agent_dqn(states_size, actions_size, memory, policy):
    model = build_model(states_size, actions_size)
    dqn = DQNAgent(model=model, nb_actions=actions_size, memory=memory, nb_steps_warmup=10,
                   target_model_update=1e-2, policy=policy)
    return dqn


def build_agent_ddpg(observation_shape, action_shape, memory):
    action_size = action_shape[0]
    actor = build_actor_model(action_size, observation_shape)
    critic, critic_action_input = build_critic_model(action_shape, observation_shape)
    random_process = OrnsteinUhlenbeckProcess(theta=.15, mu=0., sigma=.3, size=action_size)
    ddpg = DDPGAgent(nb_actions=action_size, actor=actor, critic=critic, critic_action_input=critic_action_input,
                     memory=memory, target_model_update=1e-2, nb_steps_warmup_actor=10, nb_steps_warmup_critic=10,
                     random_process=random_process
                     )
    return ddpg


def build_callbacks(agen_name):
    tensorboardcallback = TensorBoard(log_dir='./tensorboard/' + agen_name)
    callbacks = [tensorboardcallback]
    return callbacks


def print_score(score, display=True):
    print(score.history)
    wins = len([num for num in score.history['episode_reward'] if num >= 0])
    print('win % =', 100 * wins / len(score.history['episode_reward']))
    if display:
        fig, axes = plt.subplots(nrows=1, ncols=2, facecolor='white')
        ax1 = axes[0]
        ax2 = axes[1]
        ax1.set_xlabel('episodes', fontsize=8)
        ax2.set_xlabel('episodes', fontsize=8)
        ax1.set_ylabel('episode_reward', fontsize=8)
        ax2.set_ylabel('nb_steps', fontsize=8)
        ax1.set_title('episode_reward', fontsize=8)
        ax2.set_title('nb_steps', fontsize=8)
        x = np.arange(1, len(score.history['episode_reward']) + 1)
        y = score.history['episode_reward']
        # calc the trendline
        z = np.polyfit(x, y, 1)
        p = np.poly1d(z)
        ax1.plot(x, y, x, p(x))
        ax2.plot(x, score.history['nb_steps'])
        plt.tight_layout()
        plt.show()


def main():
    value = ''
    action_modes = AweleEnv.metadata['train.modes']
    while value != 'q':
        action_mode = input('1 - Discrete, 2 - Continuous :')
        if action_mode == '1':
            env = gym.make('awele-v0', action_mode=action_modes[0])
            print(env.observation_space.sample())
            # states_size = env.observation_space['board'].shape[0] + env.observation_space['possible_actions'].shape[0]
            states_size = env.observation_space.shape[0]
            print('States', states_size)
            actions_size = env.action_space.n
            print('Actions', actions_size)
            print(env.action_space.sample())
            agent_value = input('1 - DQN, 2 - SARSA :')
        elif action_mode == '2':
            env = gym.make('awele-v0', action_mode=action_modes[1])
            print(env.observation_space.sample())
            # states_size = env.observation_space['board'].shape[0] + env.observation_space['possible_actions'].shape[0]
            states_size = env.observation_space.shape[0]
            print('States', states_size)
            actions_size = env.action_space.shape[0]
            print('Actions', actions_size)
            print(env.action_space.sample())
            agent_value = input('3 - DDPG, 2 - ????? :')
        else:
            break

        if agent_value == '1':
            agent_name = 'DQN'
            nb_step = 50000
            policy = LinearAnnealedPolicy(EpsGreedyQPolicy(), attr='eps', value_max=1., value_min=.1, value_test=.05,
                                          nb_steps=10000)
            memory = SequentialMemory(limit=10000, window_length=1)
            agent = build_agent_dqn(states_size, actions_size, memory, policy)
            agent.compile(Adam(lr=1e-4), metrics=['mae'])

        elif agent_value == '2':
            agent_name = 'SARSA'
            nb_step = 50000
            policy = BoltzmannQPolicy(tau=1., clip=(-500., 500.))
            agent = build_agent_sarsa(states_size, actions_size, policy)
            agent.compile(Adam(lr=1e-4), metrics=['mae'])

        elif agent_value == '3':
            agent_name = 'DDPG'
            nb_step = 50000
            memory = SequentialMemory(limit=10000, window_length=1)
            agent = build_agent_ddpg(env.observation_space.shape, env.action_space.shape, memory)
            agent.compile(Adam(lr=1e-4), metrics=['mae'])
        else:
            break

        value = input('1 - learn, 2 - batch learning, 3 - play,  q - quit :')

        if value == '1':
            agent.fit(env, nb_steps=nb_step, visualize=False, verbose=2, callbacks=build_callbacks(agent_name))
            agent.save_weights(agent_name + '_awele.h5', overwrite=True)
            scores = agent.test(env, nb_episodes=100, visualize=False)
            print_score(scores)

        elif value == '2':
            for nb_steps in range(10000, 100000, 5000):
                agent.fit(env, nb_steps=nb_steps, visualize=False, verbose=0)
                scores = agent.test(env, nb_episodes=1000, visualize=False, verbose=0)
                print('score for nb step=', nb_steps)
                print_score(scores, display=False)

        elif value == '3':
            agent.load_weights(agent_name + '_awele.h5')
            psouth = AwelePlayer('Human South', display=True)
            pnorth = BotAwelePlayer(agent_name +' botplayer North', agent, display=False)
            board = Awele_Board(pnorth, psouth, displaybool=True)
            board.play()
            print()
            print('Count move :', board.countmoves)
            print(pnorth.name, ' score:', pnorth.score)
            print(psouth.name, ' score:', psouth.score)
            if pnorth.score > psouth.score:
                print(pnorth.name, ' is the winner')
            else:
                print(psouth.name, ' is the winner')
        else:
            break


if __name__ == '__main__':
    main()
