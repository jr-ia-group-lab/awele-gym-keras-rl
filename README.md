###### README for gym awele game

Distribution:  
./keras_rl_awele.py (bot training)  
./README.md (this file)  
./setup.py (pip install -e . to install gym-awele as a package)  
./gym-awele/__init__.py (for packaging)  
./gym-awele/envs/__init__.py (imports)  
./gym-awele/envs/awele_env.py (gym env source code)  
./gym-awele/envs/aweleboard.py (awele game rules)  
./gym-awele/envs/aweleboard.py (a bunch of awele players)  

The awele game is an african game played with a wood board and seeds.

The rules of the awele games can be found here : https://www.myriad-online.com/resources/docs/awale/english/rules.htm

A Tutorial for playing awele is here : http://www.nongnu.org/awale/tutorial.html

Awele is a two player game Player North and Player South having each one a side of the board.

I've tried to implement all Awele rules into aweleboard.py : https://www.myriad-online.com/resources/docs/awale/english/rules.htm
, the main difficulties are in rule7 (Feed the opponent) and rule 8 (End of the game).

3 players have been implemented in aweleleplayers.py : 
* AwelePlayer a basic human player
* RandomAwelePlayer an automatic player who plays randomly
* HardAwelePlayer an automatic player who choose to play for systematically capture the opponent's seeds
* BotAwelePlayer an automatic player either using DQN (Double Q Neural Network) or SARSA or DDPG reinforced learning model (that have to be trained) to choose the action to make

Awele_env.py implements 'awele-vi' game that uses the gym network in order to facilitate the reinforcement learning of the Bot Player.

The reward in the step methods implements 2 strategies:
* 1 - choose the north player action that maximize the player's score
* 2 - choose the north player action that maximize the number of seed in the north side of the board

First strategy : take seeds of your opponent whenever you can

Second strategy : dry your opponent from seed and build a "Krou" (more than 12 seeds) on your side. Then when you collapse your 'krou', you can feed your opponent with 1 seed and (during the second pass) you capture the seeds that you've just provided to your opponent.

Finally, keras_rl_awele.py let you train a DQN model based on the keras-rl adapted to the gym env.

It allows you also to compete an awele play with the bot you've just trained

