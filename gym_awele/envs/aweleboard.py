from gym_awele.envs import AwelePlayer


class Awele_Board:
    def __init__(self, playernorth: AwelePlayer = None, playersouth: AwelePlayer = None, displaybool=True):
        self.north = {1: 4, 2: 4, 3: 4, 4: 4, 5: 4, 6: 4}
        self.south = {1: 4, 2: 4, 3: 4, 4: 4, 5: 4, 6: 4}
        self.playersouth = playersouth
        self.playersouth.game = self
        self.playernorth = playernorth
        self.playernorth.game = self
        self.playerturns = self.playersouth
        self.displaybool = displaybool
        self.countmoves = 0

    def reset(self):
        self.north = {1: 4, 2: 4, 3: 4, 4: 4, 5: 4, 6: 4}
        self.south = {1: 4, 2: 4, 3: 4, 4: 4, 5: 4, 6: 4}
        self.playerturns = self.playersouth
        self.countmoves = 0
        self.playernorth.score = 0
        self.playersouth.score = 0

    def __copy__(self):
        copynorthplayer = self.playernorth.__copy__()
        copysouthplayer = self.playersouth.__copy__()
        newcopy = Awele_Board(copynorthplayer, copysouthplayer, False)
        newcopy.north = self.north.copy()
        newcopy.south = self.south.copy()
        if self.playerturns == self.playersouth:
            newcopy.playerturns = copysouthplayer
        else:
            newcopy.playerturns = copynorthplayer
        newcopy.countmoves = self.countmoves
        return newcopy

    def display(self, displaybool=None):
        if displaybool is None:
            displaybool = self.displaybool
        if displaybool:
            print('========== AWELE ==========')
            print('   1   2   3   4   5   6   north=', self.playernorth.score)
            print('---------------------------')
            for n in self.north.values():
                print(' |', n, end='')
            print(' |')
            print('---------------------------')
            for s in self.south.values():
                print(' |', s, end='')
            print(' |')
            print('---------------------------')
            print('   1   2   3   4   5   6   south=', self.playersouth.score)
            print('===========================')

    def get_possible_move(self):
        if self.playersouth.score >= 25 or self.playernorth.score >= 25:
            return []
        if sum(self.north.values()) == 1 and sum(self.south.values()) == 1:
            return []
        if self.playerturns == self.playersouth:
            availablessouth = [k for k, v in self.south.items() if v != 0]
            if sum(self.north.values()) == 0:
                # need to feed  north with seed
                for action in availablessouth:
                    if self.south[action] > 6 - action:
                        return [action]
                return []
            return availablessouth
        else:
            availablesnorth = [k for k, v in self.north.items() if v != 0]
            if sum(self.south) == 0:
                # need to feed south for seed
                for action in availablesnorth:
                    if self.north[action] > 6 - action:
                        return [action]
                return []
            return availablesnorth

    def move(self, move):
        self.countmoves = self.countmoves + 1
        if self.displaybool:
            print('move=', move, ' for player=', self.playerturns.name)
        if move in self.get_possible_move():
            if self.playerturns == self.playersouth:
                number_of_cells = self.south[move]
                self.south[move] = 0
                direction = 'south'
            else:
                number_of_cells = self.north[move]
                self.north[move] = 0
                direction = 'north'
            indices = move
            for i in range(number_of_cells):
                # increment according to direction
                if direction == 'south':
                    indices = indices + 1
                else:
                    indices = indices - 1
                # correct direction and indices
                if indices == 7:
                    direction = 'north'
                    indices = 6
                elif indices == 0:
                    direction = 'south'
                    indices = 1
                # # jump move
                if self.playerturns == self.playernorth and direction == 'north' and indices == move:
                    indices = indices - 1
                if self.playerturns == self.playersouth and direction == 'south' and indices == move:
                    indices = indices + 1
                # recorrect direction and indices
                if indices == 7:
                    direction = 'north'
                    indices = 6
                elif indices == 0:
                    direction = 'south'
                    indices = 1
                # harvest the seed
                if direction == 'south':
                    self.south[indices] = self.south[indices] + 1
                else:
                    self.north[indices] = self.north[indices] + 1
                # and that's it loop over an other seed
            self.collect_seed(indices, direction)
            return True
        else:
            if self.displaybool:
                print('game ended')
            self.playernorth.score = self.playernorth.score + sum(self.north.values())
            self.playersouth.score = self.playersouth.score + sum(self.south.values())
            if self.playernorth.score > self.playersouth.score:
                self.playernorth.stats['win'] = self.playernorth.stats['win'] + 1
                self.playersouth.stats['loss'] = self.playersouth.stats['loss'] + 1
            elif self.playernorth.score < self.playersouth.score:
                self.playernorth.stats['loss'] = self.playernorth.stats['loss'] + 1
                self.playersouth.stats['win'] = self.playersouth.stats['win'] + 1
            return False

    def collect_seed(self, indices, direction):
        if direction == 'south' and self.playerturns == self.playernorth and (
                self.south[indices] == 2 or self.south[indices] == 3):
            cell = self.south[indices]
        elif direction == 'north' and self.playerturns == self.playersouth and (
                self.north[indices] == 2 or self.north[indices] == 3):
            cell = self.north[indices]
        else:
            cell = 0

        while cell == 2 or cell == 3:
            self.playerturns.score = self.playerturns.score + cell
            if direction == 'south':
                self.south[indices] = 0
                indices = indices - 1
                if indices == 0:
                    break
                else:
                    cell = self.south[indices]
            else:
                self.north[indices] = 0
                indices = indices + 1
                if indices == 7:
                    break
                else:
                    cell = self.north[indices]

    def play(self):
        gameon = True
        self.playersouth.score = 0
        self.playernorth.score = 0
        while gameon:
            self.display()
            if self.get_possible_move():
                playermove = self.playerturns.move(self.get_possible_move())
                self.display()
                gameon = self.move(playermove)
            else:
                gameon = False
            if gameon:
                if self.playerturns == self.playersouth:
                    self.playerturns = self.playernorth
                else:
                    self.playerturns = self.playersouth
        self.playersouth.reward_punish_player()
        self.playernorth.reward_punish_player()

    def game_stat(self):
        self.playernorth.playerstats()
        self.playersouth.playerstats()
