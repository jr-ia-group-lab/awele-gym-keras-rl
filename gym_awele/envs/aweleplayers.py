import random
import numpy as np
from keras.utils import normalize
from rl.agents import DQNAgent


class AwelePlayer:

    def __init__(self, name, display=True):
        self.stats = {"win": 0, "loss": 0}
        self.display = display
        self.score = 0
        self.name = name
        self.game = None

    def __copy__(self):
        newcopy = AwelePlayer(self.name)
        newcopy.stats = self.stats.copy()
        newcopy.display = self.display
        newcopy.score = self.score
        newcopy.name = 'copy of ' + self.name
        return newcopy

    def move(self, choices):
        action = int(input(self.name + ' player, choose beetwen these moves ' + str(choices) + ':'))
        if action in choices:
            return action
        else:
            print('wrong move')
            return 0

    def playerstats(self):
        total_games = self.stats["win"] + self.stats["loss"]

        winpct = self.stats["win"] / total_games * 100
        losspct = self.stats["loss"] / total_games * 100

        print("Results for player %s:" % self.name)
        print("Wins: %d (%.1f%%)" % (self.stats["win"], winpct))
        print("Loss: %d (%.1f%%)" % (self.stats["loss"], losspct))

    def reward_punish_player(self):
        if self.display:
            print('Player ', self.name, 'score ', self.score)


class RandomAwelePlayer(AwelePlayer):
    def __init__(self, name, display=False):
        super().__init__(name, display)

    def move(self, choices):
        if choices:
            return random.choice(choices)
        else:
            return 0


class HardAwelePlayer(AwelePlayer):
    def __init__(self, name, display=False):
        super().__init__(name, display)

    def move(self, choices):
        if choices:
            choosedaction = random.choice(choices)
            thedisplay = 'Random choice =' + str(choosedaction)
            currentscore = self.score
            if self.display:
                print('current score=', currentscore)
                print('COPY GAME=================================================================================')
            for action in choices:
                copygame = self.game.__copy__()
                copygame.move(action)
                if self.display:
                    print('compare current score with copy score=', copygame.playerturns.score, ' for action:', action)
                if copygame.playerturns.score > currentscore:
                    choosedaction = action
                    currentscore = copygame.playerturns.score
                    thedisplay = 'hard choice =' + str(choosedaction)
            if self.display:
                print(thedisplay)
                print('END COPY GAME=================================================================================')
            return choosedaction
        else:
            return 0


class BotAwelePlayer(AwelePlayer):
    def __init__(self, name, dqn: DQNAgent, display=True):
        super().__init__(name, display)
        self.dqn = dqn

    def move(self, choices):
        # possible_actions = np.array(choices)
        board = np.array(
            [seeds for seeds in self.game.south.values()] + [seeds for seeds in self.game.north.values()])
        # zeros = np.zeros(6 - possible_actions.size)
        # state = np.concatenate((board, possible_actions, zeros), axis=None)
        # state = np.concatenate((board, possible_actions, zeros), axis=None)
        state = normalize(board, axis=-1, order=2)[0]
        action = self.dqn.forward(state)
        if action in choices:
            if self.display:
                print(self.name, ' player predict=', action)
        else:
            action = random.choice(choices)
            if self.display:
                print(self.name, ' player random choice=', action)
        return action
