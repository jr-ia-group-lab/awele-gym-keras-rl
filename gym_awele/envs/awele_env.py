import random

import gym
import numpy as np
from gym import spaces
from gym.utils import seeding
from keras.utils import normalize

from gym_awele.envs import HardAwelePlayer, Awele_Board, AwelePlayer


class AweleEnv(gym.Env):
    """
        Description:
            The awele game is an african game played with a wood board and seeds.
            it is a 2 players game.
            4 different players are provided Human, Random, Hard and Bot players.
.
        Source:
            JR GRUSER

        Observation:
            Type: Box(12)
            Key                 Observation                                   Min             Max         Shape
            board               number of seeds for the 2*6 holes             0               12          12 integers
        Actions:
            Type: Discrete(6) or Box(6)
            Action
            1-6
            Note : players choose the hole number according possible action observation.
        Reward:
            Reward is +1 if seeds are taken and -1 if the opponent take seeds.
            Reward is +1 if total seeds is superior of the total seeds of the opponent.
        Starting State:
            Every 12 holes are provided with 4 seeds.
        Episode Termination:
            no more seeds can be taken or one of the player gets 25 seeds
        """
    metadata = {'render.modes': ['human', 'bot'], 'train.modes': ['discrete', 'continuous']}

    def __init__(self, action_mode):
        self.action_mode = action_mode
        if action_mode == self.metadata['train.modes'][0]:
            self.action_space = spaces.Discrete(6)
        elif action_mode == self.metadata['train.modes'][1]:
            self.min_action = 1.
            self.max_action = 6.
            self.action_space = spaces.Box(np.array([self.min_action]), np.array([self.max_action]), shape=(1,),
                                           dtype=np.float32)
        else:
            return

        self.observation_space = spaces.Box(low=0, high=48, shape=(12,), dtype=np.int)
        # self.observation_space = spaces.Dict({'board': spaces.Box(low=0, high=48, shape=(12,), dtype=np.int),
        # 'possible_actions': spaces.Box(low=1, high=6, shape=(6,), dtype=np.int)})
        self.botplayernorth = AwelePlayer('Bot North', display=False)
        playersouth = HardAwelePlayer('Hard South', display=False)
        self.board = Awele_Board(self.botplayernorth, playersouth, displaybool=False)
        self.state = self.get_normalize_state()
        # self.state = np.concatenate((np.array([[4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]]), np.array([[1, 2, 3, 4, 5,
        # 6]])), axis=None)
        self.reward = 0
        self.count_random = 0
        self.count_predict = 0
        self.seed()

    def seed(self, seed=None):
        _, seed = seeding.np_random(seed)
        return [seed]

    def get_normalize_state(self):
        # possible_actions = np.array(self.board.get_possible_move())
        board = np.array(
            [seeds for seeds in self.board.south.values()] + [seeds for seeds in self.board.north.values()])
        # zeros = np.zeros(6 - possible_actions.size)
        # state = np.concatenate((board, possible_actions, zeros), axis=None)
        return normalize(board, axis=-1, order=2)[0]
        # return board

    def step(self, action):
        if not self.board.get_possible_move():
            done = True
            info = {'End game playerturn': 'player ' + self.board.playerturns.name + ' is playing ' + str(action)}

        elif self.board.playerturns == self.botplayernorth:
            if self.action_mode == self.metadata['train.modes'][0]:
                action = int(str(action)) + 1
            elif self.action_mode == self.metadata['train.modes'][1]:
                action = round(np.clip(action[0], self.min_action, self.max_action))
            else:
                print('action error ', str(action))
                return

            # store score before move
            scorenorth = self.board.playernorth.score
            seednumbernorth = sum(self.board.north.values())
            scoresouth = self.board.playersouth.score
            seednumbersouth = sum(self.board.south.values())

            if action not in self.board.get_possible_move():
                action = random.choice(self.board.get_possible_move())
                self.count_random = self.count_random + 1
                info = {'playerturn': 'player ' + self.board.playerturns.name + ' is randomly playing ' + str(
                    action),
                        'count_random': self.count_random}
            else:
                self.count_predict = self.count_predict + 1
                info = {'playerturn': 'player ' + self.board.playerturns.name + ' is playing ' + str(action),
                        'count_predict': self.count_predict}

            done = not (self.board.move(action))

            if not done:
                self.state = self.get_normalize_state()
                self.reward = self.get_reward(scorenorth, seednumbernorth, scoresouth, seednumbersouth)
                info['reward'] = self.reward
        else:
            action = self.board.playerturns.move(self.board.get_possible_move())
            info = {'playerturn': 'player ' + self.board.playerturns.name + ' is playing ' + str(action)}
            done = not (self.board.move(action))

        if not done:
            info['score'] = self.board.playerturns.score
            info['done'] = done
            self.switch_player()
        else:
            if self.board.playernorth.score > self.board.playersouth.score:
                # self.reward = self.reward + 1
                self.reward = 1
            else:
                self.reward = - 1
        # print(info)
        return self.state, self.reward, done, info

    def switch_player(self):
        if self.board.playerturns == self.board.playersouth:
            self.board.playerturns = self.board.playernorth
        else:
            self.board.playerturns = self.board.playersouth

    def get_reward(self, scorenorth, seednumbernorth, scoresouth, seednumbersouth):
        newscorenorth = self.board.playernorth.score
        newsseednumbernorth = sum(self.board.north.values())
        newscoresouth = self.board.playersouth.score
        newsseednumbersouth = sum(self.board.south.values())

        # reward score north
        if newscorenorth > scorenorth:
            reward = 10
        elif newscoresouth > scoresouth:
            reward = -10
        else:
            reward = 0
        # reward seed number north
        if newsseednumbernorth > seednumbernorth:
            reward = reward + 5
        elif newsseednumbersouth > seednumbersouth:
            reward = reward - 5
        else:
            pass

        # reward seed 1 or 2
        countnorth = [k for k, v in self.board.north.items() if v == 1 or v == 2].__sizeof__()
        countsouth = [k for k, v in self.board.south.items() if v == 1 or v == 2].__sizeof__()
        if countnorth < countsouth:
            reward = reward + 5
        elif countnorth > countsouth:
            reward = reward - 5
        return reward / 20

    def reset(self):
        self.board.reset()
        self.state = self.get_normalize_state()
        # self.state = np.concatenate((np.array([[4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4]]), np.array([[1, 2, 3, 4, 5,
        # 6]])), axis=None)
        self.reward = 0
        self.count_random = 0
        self.count_predict = 0
        return self.state

    def render(self, mode='human', close=False):
        if mode == self.metadata['render.modes'][0]:
            self.board.display(displaybool=True)
        else:
            super(self).render(mode=mode)  # just raise an exception
            raise BaseException('error render mode')

    def possible_actions(self):
        actions = self.board.get_possible_move()
        player = self.board.playerturns
        return player, actions

    # def set_player(self, playernorth: AwelePlayer, playersouth: AwelePlayer):
    #     self.board.playersouth = playersouth
    #     self.board.playernorth = playernorth
