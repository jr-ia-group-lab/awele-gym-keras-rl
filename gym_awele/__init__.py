from gym.envs.registration import register

register(
    id='awele-v0',
    entry_point='gym_awele.envs:AweleEnv',
)
